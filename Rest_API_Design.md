
## How to create REST APIs using best practices

 1. I**dentify Object Model** - means identify the *objects to be presented as resources*.
 2. **Create Model URIs** - requires us to identify the relationships among the resources and sub-resources if any. And *URIs should only include nouns, verbs should be strictly avoided.*
 3. **Determine Representations** - Data that will be returned using the URIs should be small and relevant to the purpose. Data can be return in various representations based on the purpose of the URIs, which are described as below-

	 - Collection of Resource - Include only the most important data which  will keep the payload size small and will improve performance.
	 - Single Resource - Complete information of the resource, links to the sub-resources and other supported operations should be returned.
	 - Collection of Sub-resource - Similar to the resource collection minimal info should be returned.
	 - Single Sub-resource - Should return full information about the sub-resource.
	 - Collection of Sub-resources under Single Resource - Data returned will the subset of primary collection of sub-resources.
	 - Single Sub-resource under Single Resource - Data returned of the resource can be full or partial information according to the need

4. **Assign HTTP Methods** 
***Browsing all resources or sub-resources***
*HTTP GET /resources
HTTP GET /sub-resources*

***Browsing all sub-resources of the resource***
*HTTP GET /resources/{id}/sub-resources*

***Browsing single resource***
*HTTP GET /resources/{id}*

***Browsing single sub-resource***
*HTTP GET /sub-resources/{id}*

***Browsing single sub-resource of a resource***
*HTTP GET /sub-resources/{id}/sub-resources/{id}*

***Adding a resource or sub-resource***
*HTTP POST /resources
HTTP POST /sub-resources*

***Updating a resource or sub-resource***
*HTTP PUT /resources/{id}
HTTP PUT /sub-resources/{id}*

***Deleting a resource or sub-resource***
*HTTP DELETE /resources/{id}
HTTP DELETE /sub-resources/{id}*

***Deleting or updating a particular sub-resource from a resource***
*HTTP PUT /resources/{id}/sub-resources
HTTP DELETE /resources/{id}/sub-resources/{id}*  



 
