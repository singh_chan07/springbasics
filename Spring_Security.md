﻿# Spring Security Basics


## Spring Security Jargon



**SecurityContext** :  holds the Authentication and request-specific security information.

**SecurityContextHolder**: stores details of the present security context of the application, which includes details of the principal currently using the application.


> *By default it uses *ThreadLocal* to store these details which means that security context is always available to methods in the same thread of execution, even if the security context is not passed around as an argument to those methods*



**Authentication**: represents an object to represent information of the current principal. 

**Principal**: Principal is just an Object obtained from Authentication. It can be cast to *UserDetails* object.

**UserDetails** : is a core interface in Spring Security. It represents a principal, but in a extensible and application-specific way. It acts as the adapter between our own user instance and what spring needs inside the *SecurityContextHolder,* It can be cast to to original object provided by the application to execute business tasks.

**UserDetailsService** : Interface with only one method that returns *userDetails* object. And it On successful authentication *userDetails* is used to build Authentication object that is placed in *SecurityContextHolder*.


**GrantedAuthorities** : Represents an authority that is granted to the principal, such authorities usually are roles. These roles are later on configured for web authorization, method authorization and domain object authorization. Spring Security interprets these authorities and expect them be to be present. *GrantedAuthority* object are usually loaded by the *UserDetailsService*.

> GrantedAuthority objects are application-wide permissions. They are not specific to a given domain object.

**UsernamePasswordAuthenticationToken**- Instance of Authentication which combines Username and password 

**AuthenticationManager** - validates the token(authentication token) and returns a fully populated authentication object on successful completion.

**AccessDecisionManager** - responsible for making access-control decisions in Spring Security.

**DelegatingFilterProxy** 
Proxy for a standard Servlet 2.3 Filter, delegating to a Spring-managed bean that implements the Filter interface. Supports a "*targetBeanName*" filter *init-param* in *web.xml*, specifying the name of the target bean in the Spring application context.
*web.xml* will usually contain a *DelegatingFilterProxy* definition, with the specified filter-name corresponding to a bean name in Spring's root application context. All calls to the filter proxy will then be delegated to that bean in the Spring context, which is required to implement the standard Servlet 2.3 Filter interface

**FilterChainProxy** 
Delegates Filter requests to a list of Spring-managed filter beans. 
The FilterChainProxy is linked into the servlet container filter chain by adding a standard Spring DelegatingFilterProxy declaration in the application *web.xml* file.

## Authentication 

*AuthenticationManager* is an interface, *ProviderManager* is its default implementation in Spring Security, which delegates the authentication request to list of configured *AuthenticationProvider*s, each of which is queried to see if it can perform authentication. Each provider will either throw exception or return a fully populated Authentication object. The most common approach to verifying an authentication request is to load the corresponding *UserDetails* and check the loaded password against the one that has been entered by the user. This is the approach used by the *DaoAuthenticationProvider*. The loaded *UserDetails* object - and particularly the *GrantedAuthority*s it contains - will be used when building the fully populated Authentication object which is returned from a successful authentication and stored in the *SecurityContext*.


## Access-Control(Authorization)

*AccessDecisionManager* - responsible for making access-control decisions in Spring Security. It has a decide method which takes and Authentication object representing the principal requesting access a "*secure object*" and a list of security metadata attributes which apply for the object(such a list of roles which are required for the access to be granted)





## Spring Security Configuration

> @EnableWebSecurity with WebSecurityConfigurerAdapter configures spring security for the application

**HttpSecurity** - It allows configuring web based security for specific http requests. By default it will be applied to all requests, but can be restricted using requestMatcher(*RequestMatcher*) or other similar methods.

**WebSecurity** - creates the FilterChainProxy and configures, it is mostly used to ignore requests

**Cross-site request forgery (CSRF)** - Cross site request forgery involves tricking a victim into making a request that utilizes their authentication or authorization. By leveraging the account privileges of a user, an attacker is able to send a request masquerading as the user. Once a user’s account has been compromised, the attacker can exfiltrate, destroy or modify important information. Highly privileged accounts such as administrators or executives are commonly targeted.









