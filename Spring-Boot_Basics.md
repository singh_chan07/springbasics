﻿# Spring Boot Basics

**What is the Spring boot starters?**
A set of convenient dependency descriptors that we can include in our application to allow spring boot manage our dependencies automatically.It is kind of one stop shop for all the spring and related technologies that you need without having to hunt through sample code and copy pase loads of dependencies. Threre are more than 30 packages.
These starters help us to reduce the number of manually added dependencies just by adding one one dependency.

 1. Web starter- contains dependencies required to create web apps (like rest applications etc..). It include various dependencies like tomcat, Jackson, Spring MVC.
 2. Test Starter - It includes Spring Test, JUnit, Hamcrest and Mockito. 
 3. Data JPA Starter - contains dependencies required for persistence. 
 4. The mail Starter - It includes mail dependencies

**What is Auto Configuration Feature in Spring Boot?**
Spring Boot automatically creates configuration beans by analyzing the class-path. 

## Annotations and their usage
*@SpringBootAnnoatation* - is a convenience annotation that adds all of the following:

 1. *@Configuration*: Tags the class as a source of bean definitions for the application context.
 2. *@EnableAutoConfiguration*: Tells spring boot to add class base on class path settings and various property settings. For ex- If spring-webmvc is on the classpath, this annotation flags the application as a web application and activates key behaviours such as setting up a Dispatcher Servlet.
 3. *@ComponentScan* : Tells spring to look for other components, configurations and services in the com/example package, letting it find controllers
	
*@Component* : is an annotation that allows spring to detect our custom beans, ie spring will scan our application for classes annotated with @Component, instantiate them and inject any specified dependencies into them, inject them wherever needed. The annotation serve the purpose to differentiate an object from others such as domain objects.

*@Repository*: It's job is to catch persistence-specific exceptions and re-throw them as one of spring's unified unchecked exceptions.

*@Service:* Marks beans to indicate that they are holding business logic.

*@Bean* : it is also an annotation used to gather beans at runtime, but it's not used at the class level, rather we annotate methods with @Bean so that spring can store the methods' result as a spring bean.

> Spring Stereotype Annotations
@Controller = @Service = @Repository = @Component
They act the same because they are composed annotations with @Component as a meta-annotation for each of them.
	
	
	
*@RequestMapping*: marks request handler methods. it can be configured using:
- *path/name/value*: specifies the url that will be mapped to the method.
 - *method:* one of the http methods.
 - *params:* filters request based on presence, absence or value of HTTP parameters
 - *headers:* filters requests based on presence absence or value of http headers
 - *consumes:* which media types the method can consume in the http request body
 -  *produces:* which media types the method can produce in the Http response body.

> @GetMapping, @PostMapping, @PutMapping, @DeleteMapping, @PatchMapping are different variants of @RequestMapping with HTTP method already set to GET, POST, DELETE, and PATCH, respectively.

*@RequestBody*: Maps body of the request to an object.

*@PathVariable*: indicates that a method argument is bound to a URI template variable We can specify the URI template with the @RequestMapping annotation and bind a method argument to one of the template parts with @PathVariable.


*@RequestParam:* used for accessing HTTP request parameters.

*@CookieValue* : can access cookies

*@RequestHeader:* can access request headers

*@ResponseBody:* marks the result of the method as the response body of the request.

*@ExceptionHandler* : allows us to declare custom error handler method. Spring calls this method when a request handler method throws any of the specified exceptions.The caught exception can be passed to the method as an argument.

*@ResponseStatus*: Specify the desired HTTP status of the response if we annotate a request handler method with this annotation. we can declare status code with the code argument or value argument and can also provide a reason using the reason argument, we can also use it along with @ExceptionHandler.

*@Controller*: defines Spring MVC controller.

*@ModelAttribute*: allows to access elements that are already in the model of an MVC @Controller, by providing the model key. We can also use it to add method's return value to model

*@CrossOrigin:* enables cross-domain communication for the annoted request handler methods.

*@Qualifier:* it is used along with @Autowired to provide the bean id or bean name in ambiguous situations.

*@Required:* marks dependecies that we want to populate through the XML.

*@Value:* We can use @Value for injecting the property values into beans. It's compatible with constructor, setter and field injections

*@Lazy:* Used when we want our beans to be lazily initialized. By default Spring creates all singleton beans eagerly at the startup/bootstrapping of the application context. It is used to create bean when it's needed, not at the application startup.It beahaves differently depending on where it is used- 1)a @Bean annotated bean factory method, to delay the method call (hence the creation). 2) a @Configuration class and all contained @Bean methods will be affected 3) a @Conmponent class which is not a @Configuration class, this bean will be initialized lazily. 4) an @Autowired constructor, setter, field , to load the dependency itself lazily(via proxy). This annotation has an argument named value with the default value of true. It is used to override the default behaviour.

*@Lookup*: a method annotated with @Lookup tells spring to return an instance of the method's return type when we invoke it.

*@Primary:* Used to mark a bean as the default in cases where there is no qualification mentioned, It's used in situations where we have specified multiple beans of same type.

*@Scope:* to define the scope of a @Component class or a @Bean definition, It can be singleton, prototype, request, session, globalSession or some custom scope.

*@Profile:* Used when we want Spring to use a @Component class or a @Bean method only when a specific profile is active. We can mark it with @Profile. We can configure the name of the profile, with the value argument of the annotation.

*@Import:* Used to use specific @Configuration classes without component scanning with this annotation. We can provide those classes with @Import's value argument.

*@ImportResource:* used to import XML configurations.

*@PropertySource:* define property files for application settings
*@PropertySources* : used to encapsulate multiple *@PropertySource* annotations.
	
	
*@Autowired:* Mark a dependency which spring is going to resolve and inject. It can be used with a constructor or setter. It has a boolean argument required with a default value of true. When true, an exception is thrown or otherwise nothing is wired.

*@RestController*: @Controller + @ResponseBody
	marks the class as a controller where every method returns a domain object instead of view
	

	
	
	
	
### Hiberanate Validator Annoations

*@NotNull:* validates that the annoatated property value is not null
*@AssertTrue:* validates that the annotated property value is true
*@Size:* validates that the annoatated property value has a size in between the attributes min and max.
*@Min:* validates the property can not have lower value than min.
*@Max*: validates tthe property can not have higher value than max.

*@Email:* validates that the annoatated property is a valid email address.

*@NotEmpty:* validates that the property is not null or empty can be applied to String , Collection , Map, Array Values.

*@NotBlank:* can be applied only to text values and validates that the property is not blank.

*@Positive and @PositiveOrZero* apply to numeric values and validate that they are strictly positive, or positive including 0.

*@Negative and @NegativeOrZero* apply to numeric values and validate that they are strictly negative, or negative including 0.

*@Past and @PastOrPresent* validate that a date value is in the past or the past including the present; can be applied to date types including those added in Java 8.

*@Future and @FutureOrPresent* validate that a date value is in the future, or in the future including the present.





















