﻿# Spring Data JPA

## Associations
A mechanism to represent relationships between entity types.
**Unidirectional** : refers when the association only allows either the parent entity or child entity to access the other but not the vice versa.
**Bidirectional** : refers when the association allows both the parent and child to refer/access each other.

### ManyToOne
An association in which a foreign-key column is added in the database table for the entity with *@ManyToOne* annotation. For bidirectional *@OneToMany* annotation need to be used on the parent entity. This association when unidirectional just creates a column for foreign key references in the child, the bidirectional is same as the *@OneToMany* bidirectional.

### OneToMany
Unidirectional creates a link table and allows the parent to traverse the child entity properties. Bidirectional requires to add *@ManyToOne* on the child side, and this only creates a foreign key column in the child side.

### OneToOne
The annotation *@OneToOne* in both unidirectional, bidirectional, like manyToOne add a foreign key column to the child/client side. A unidirectional association follows the relational database foreign key semantics, the client-side owning the relationship. A bidirectional association features a ***mappedBy*** *@OneToOne* on parent side too.

### ManyToMany
Unidirectional, bidirectional both will create link/join table.

> Every bidirectional association has an owning side and the other is called inverse side and ***mappedBy*** is used to represent the inverse side.

## Mapping Inheritance

1. ***@MappedSuperClass***: Is used to group common properties in the base entity. And inheritance is visible in the domain model only, each database table contains both the base class and the subclass properties.
2. ***Single Table***: maps all subclasses to only one database table. Each subclass declares its own persistent properties. But requires to define a ***descriminator*** value, which hibernate uses to differentiate which row belongs to which class.
3.  ***Joined table*** Each subclass is mapped to its own table. This is also called  _table-per-subclass_  mapping strategy. An inherited state is retrieved by joining with the table of the superclass. This requires us to declare a table column holding the object identifier.
4. ***Table per class***: map only the concrete classes of an inheritance hierarchy to tables. Each table defines all persistent states of the class, including the inherited state.

## Query in Spring Data JPA
***@Query*** : its value attribute contains the JPQL or SQL to execute.The *@Query* annotation takes precedence over named queries, which are annotated with *@NamedQuery* or defined in an orm.xml file.

 - By default, the query definition uses JPQL. 	 
 - To use native SQL to define our query, we need to set the value of the nativeQuery attribute to true and define the native SQL query in the value attribute of the annotation.
 - Sort can be passed as parameter to method annotated with *@Query* to sort results, and it gets translated to **ORDER BY.**
 - When we use JPQL for a query definition, then Spring Data can handle sorting without any problem. It's crucial that we use JpaSort.unsafe() to create a Sort object instance. With native enabled *@Query* it is not possible to sort results. To enable pagination for native queries by declaring an additional attribute countQuery.
 - Indexed parameters for JPQL and Native SQL query, Spring Data will pass method parameters to the query in the same order they appear in the method declaration.
 - For both JPQL and Native SQL, Spring data pass method parameters to the query using named parameters. We define these using the *@Param* annotation inside our repository method declaration. Each parameter annotated with *@Param* must have a value string matching the corresponding JPQL or SQL query parameter name.
 - We can use the *@Query* annotation to modify the state of the database by also adding the *@Modifying* annotation to the repository method.
	
### Spring Derived Query Methods
Derived method names have two main parts separated by the first  _By_ keyword. The first part is called the _introducer_ and the rest  is called the _criteria._
 - Spring Data JPA supports  _find, read, query, count_  and _get_ introducers. 
 - The criteria part contains the entity-specific condition expressions of the query.


